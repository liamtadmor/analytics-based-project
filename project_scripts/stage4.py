import pandas as pd
import numpy as np
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.ensemble import GradientBoostingRegressor, RandomForestClassifier
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score, roc_curve
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
from statsmodels.graphics.gofplots import qqplot
from imblearn.over_sampling import SMOTE

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'  # directory of data file

# # # Read relevant files # # #

df = pd.read_csv(f'{files_directory}/stage3_after_nas.csv', index_col=0)
df['loan_status'] = df['loan_status'].astype(int)

# # # Checking some more correlation between the features # # #
fig, ax = plt.subplots(figsize=(40, 20))
sns.heatmap(df.corr(method='pearson'), annot=True)
plt.title("Full Pearson Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / 'full_corr.png')

# # # Features Cleaning based on correlation # # #
cols1, cols2, cols_tuple, pearson_corr = [], [], [], []
for col in df.columns:
    for col2 in df.columns:
        cols1.append(col)
        cols2.append(col2)
        cols_tuple.append((col, col2))
        pearson_corr.append(pearsonr(df[col], df[col2])[0])

for i in range(len(cols_tuple)):
    cols_tuple[i] = tuple(sorted(cols_tuple[i]))
df_corr = pd.DataFrame({'col1': cols1, 'col2': cols2, 'cols_tup': cols_tuple, 'pearson_correlation': pearson_corr})
df_corr = df_corr[df_corr['col1'] != df_corr['col2']]  # dropping irrelevant correlation
df_corr = df_corr.drop_duplicates(subset='cols_tup')  # dropping duplicates rows
df_corr1 = df_corr[np.abs(df_corr['pearson_correlation']) > 0.75]  # all pairs of features with correlation above 0.75

# checking the correlation of ll featres with loan_status and expected return
columns, pearson_corr_status, pearson_corr_return = [], [], []
for col in df.columns:
    columns.append(col)
    pearson_corr_status.append(pearsonr(df[col], df['loan_status'])[0])
    pearson_corr_return.append(pearsonr(df[col], df['ann_exp_return'])[0])

df_corr_return = pd.DataFrame({'columns': columns, 'pearson_correlation_with_status': pearson_corr_status,
                               'pearson_correlation_with_return': pearson_corr_return})

# dropping highly correlated features with others
to_drop_corr_pearson = ['fico_range_high', 'installment', 'tot_hi_cred_lim', 'tot_cur_bal', 'mths_from_ear_credit',
                        'total_il_high_credit_limit', 'total_bc_limit', 'revol_bal', 'grade']

df_after_corr = df.drop(to_drop_corr_pearson, axis=1)

fig, ax = plt.subplots(figsize=(40, 20))
sns.heatmap(df_after_corr.corr(method='pearson'), annot=True)
plt.title("New Full Pearson Correlation", fontsize=20)
plt.show()
# plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / 'new_full_corr.png')

# # # Handling outliers # # #

# checking first if the features are normally distributed
for col in df_after_corr.columns:
    qqplot(df_after_corr[col], line='s')
    plt.title(col)
    plt.show()

# instances anomaly detection with KNN
df_anomaly = df_after_corr.copy()
NN = NearestNeighbors()
NN.fit(df_anomaly)

distances, indexes = NN.kneighbors(df_anomaly)
mean_distances = pd.DataFrame(distances.mean(axis=1), columns=['dists'])
mean_distances.index = df_anomaly.index
plt.plot(mean_distances['dists'].tolist())
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'anomaly_outliers.png')

dist_thresh = 25000
outlier_index = mean_distances[mean_distances['dists'] > dist_thresh].index.tolist()
inlier_index = mean_distances[mean_distances['dists'] <= dist_thresh].index.tolist()
print(len(outlier_index))
# outlier_values = df.reset_index().iloc[outlier_index]
# print(outlier_values)

df_after_anomalies = df_after_corr.loc[inlier_index, :]
df_after_anomalies.to_csv(f'{files_directory}/stage4_after_anomalies.csv')

df1 = pd.read_csv(f'{files_directory}/stage4_after_anomalies.csv', index_col=0)
df1['loan_status'] = df1['loan_status'].astype(int)

# # # Train/Test split + Scaling # # #

X_reg = df1.drop(['loan_status', 'ann_exp_return'], axis=1)
y_reg = df1['ann_exp_return']
Xr_train, Xr_test, yr_train, yr_test = train_test_split(X_reg, y_reg, test_size=0.4, random_state=0)

scaler_reg = StandardScaler()
Xr_train_sc = pd.DataFrame(scaler_reg.fit_transform(Xr_train))
Xr_train_sc.columns = Xr_train.columns
Xr_test_sc = pd.DataFrame(scaler_reg.transform(Xr_test))
Xr_test_sc.columns, Xr_test_sc.index = Xr_test.columns, Xr_test.index

X_clf = df1.drop(['loan_status', 'ann_exp_return'], axis=1)
y_clf = df1['loan_status']
y_return = df1['ann_exp_return']
Xc_train, Xc_test, yc_train, yc_test, y_return_train, y_return_test = \
    train_test_split(X_clf, y_clf, y_return, test_size=0.4, stratify=y_clf, random_state=0)

smote_clf = SMOTE(sampling_strategy=(0.5/0.5))
print('Total instances before smote: %d' % len(Xc_train.index))
Xc_smt, yc_smt = smote_clf.fit_resample(Xc_train, yc_train)
print('Total instances after smote: %d' % len(Xc_smt.index))
print(yc_smt.value_counts())

plt.pie(yc_train.value_counts(), colors=['c', 'b'], labels=['Fully Paid', 'Charged Off'], autopct='%1.2f%%')
plt.title('Imbalanced Data')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'pie_before_smote.png')
plt.pie(yc_smt.value_counts(), colors=['c', 'b'], startangle=90, labels=['Fully Paid', 'Charged Off'], autopct='%1.2f%%')
plt.title('Balanced Data')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'pie_after_smote.png')


scaler_clf = StandardScaler()
Xc_train_sc = pd.DataFrame(scaler_clf.fit_transform(Xc_train))
Xc_train_sc.columns = Xc_train.columns
# Xc_smt_sc = pd.DataFrame(scaler_clf.fit_transform(Xc_smt))
# Xc_smt_sc.columns = Xc_smt.columns
Xc_test_sc = pd.DataFrame(scaler_clf.transform(Xc_test))
Xc_test_sc.columns, Xc_test_sc.index = Xc_test.columns, Xc_test.index

# # # Regression Model # # #

def train_reg(model, X_train, y_train, X_test, y_test):
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    print(f'{model} R-squared: %.4f' % r2_score(y_test, y_pred))
    print(f'{model} RMSE: %.4f' % mean_squared_error(y_test, y_pred, squared=True))
    return y_pred

def results_reg(y_pred, y_test, X_test, threshold):
    returns_of_loans_to_inv = y_test[y_pred > threshold]
    amnt_inv = X_test[y_pred > threshold].iloc[:, 0]
    tot_inv = np.sum(amnt_inv)
    tot_loans_inv = len(amnt_inv)
    avg_wght_return = np.sum((amnt_inv / tot_inv) * returns_of_loans_to_inv)
    sd_wght_return = np.sqrt(np.sum(pow((returns_of_loans_to_inv - avg_wght_return), 2)) / len(returns_of_loans_to_inv))
    return avg_wght_return, sd_wght_return, tot_inv, tot_loans_inv

LR = LinearRegression()
y_pred_lr = train_reg(LR, Xr_train_sc, yr_train, Xr_test_sc, yr_test)
GBR = GradientBoostingRegressor()
y_pred_gbr = train_reg(GBR, Xr_train, yr_train, Xr_test, yr_test)

thresholds_reg = np.arange(0, 0.08, 0.01)
avg_wghtd_rs_lr, sd_wghtd_rs_lr, tot_invs_lr, tot_loans_invs_lr = [], [], [], []
avg_wghtd_rs_gbr, sd_wghtd_rs_gbr, tot_invs_gbr, tot_loans_invs_gbr = [], [], [], []

for thresh in thresholds_reg:
    avg_wght_return_lr, sd_wght_return_lr, tot_inv_lr, tot_loans_inv_lr = results_reg(y_pred_lr, yr_test, Xr_test, thresh)
    avg_wghtd_rs_lr.append(avg_wght_return_lr)
    sd_wghtd_rs_lr.append(sd_wght_return_lr)
    tot_invs_lr.append(tot_inv_lr)
    tot_loans_invs_lr.append(tot_loans_inv_lr)
    avg_wght_return_gbr, sd_wght_return_gbr, tot_inv_gbr, tot_loans_inv_gbr = results_reg(y_pred_gbr, yr_test, Xr_test, thresh)
    avg_wghtd_rs_gbr.append(avg_wght_return_gbr)
    sd_wghtd_rs_gbr.append(sd_wght_return_gbr)
    tot_invs_gbr.append(tot_inv_gbr)
    tot_loans_invs_gbr.append(tot_loans_inv_gbr)

reg_avg_returns_df = pd.DataFrame({
    'Avg Weighted Return LR': avg_wghtd_rs_lr, 'SD Returns LR': sd_wghtd_rs_lr,
    'Amount Invested LR': tot_invs_lr, 'Loans invested LR': tot_loans_invs_lr,
    'Avg Weighted Return GBR': avg_wghtd_rs_gbr, 'SD Returns GBR': sd_wghtd_rs_gbr,
    'Amount Invested GBR': tot_invs_gbr, 'Loans invested GBR': tot_loans_invs_gbr
})
reg_avg_returns_df.index = thresholds_reg

# # # Classification model # # #

def train_clf(X_train, y_train, X_test, model):
    model.fit(X_train, y_train)
    y_prob = model.predict_proba(X_test)[:, 1]
    return y_prob

def results_clf(y_prob, threshold, X_test, y_test, y_return, print_results=False):
    y_pred = (y_prob > threshold)
    recall = recall_score(y_test, y_pred)
    if print_results:
        print('Clf recall: %.4f' % recall)
        confusion_matrix_pred = pd.DataFrame(confusion_matrix(y_test, y_pred),
                                             columns=pd.MultiIndex.from_product([['Prediction'], ['Fully Paid', 'Charged Off']]),
                                             index=pd.MultiIndex.from_product([['Actual'], ['Fully Paid', 'Charged Off']]))
        print(confusion_matrix_pred)

    returns_of_loans_to_inv = y_return[y_pred == 0]
    amnt_inv = X_test[y_pred == 0].iloc[:, 0]
    tot_inv = np.sum(amnt_inv)
    tot_loans_inv = len(amnt_inv)
    avg_wght_return = np.sum((amnt_inv/tot_inv) * returns_of_loans_to_inv)
    sd_wght_return = np.sqrt(np.sum(pow((returns_of_loans_to_inv-avg_wght_return), 2))/len(returns_of_loans_to_inv))

    return avg_wght_return, sd_wght_return, tot_inv, tot_loans_inv, recall

def plot_roc_curve(y_test, y_probs, colors, labels):
    plt.figure(figsize=(10, 6))
    plt.plot([0, 1], [0, 1], 'r--')
    for i in range(len(y_probs)):
        fpr, tpr, threshs = roc_curve(y_test, y_probs[i])
        plt.plot(fpr, tpr, colors[i], label=labels[i])
        plt.text(0.6, 0.2-(i*4/100), f'AUC {labels[i]}: {round(roc_auc_score(y_test, y_probs[i]), 2)}')
    plt.legend()
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

LG = LogisticRegression()
y_prob_lg = train_clf(Xc_train_sc, yc_train, Xc_test_sc, LG)
# y_prob_lg = train_clf(Xc_smt_sc, yc_smt, Xc_test_sc, LG)
RFC = RandomForestClassifier()
y_prob_rfc = train_clf(Xc_train, yc_train, Xc_test, RFC)
# y_prob_rfc = train_clf(Xc_smt, yc_smt, Xc_test, RFC)
KNN = KNeighborsClassifier()
y_prob_knn = train_clf(Xc_train_sc, yc_train, Xc_test_sc, KNN)
# y_prob_knn = train_clf(Xc_smt_sc, yc_smt, Xc_test_sc, KNN)

thresholds = np.round(np.arange(0.1, 0.51, 0.05), 2)
avg_wghtd_rs_lg, sd_wghtd_rs_lg, tot_invs_lg, tot_loans_invs_lg, recalls_lg = [], [], [], [], []
avg_wghtd_rs_rfc, sd_wghtd_rs_rfc, tot_invs_rfc, tot_loans_invs_rfc, recalls_rfc = [], [], [], [], []
avg_wghtd_rs_knn, sd_wghtd_rs_knn, tot_invs_knn, tot_loans_invs_knn, recalls_knn = [], [], [], [], []

for thresh in thresholds:
    avg_wghtd_r_lg, sd_wghtd_r_lg, tot_inv_lg, tot_loans_inv_lg, recall_lg = results_clf(y_prob_lg, thresh, Xc_test, yc_test, y_return_test, False)
    avg_wghtd_rs_lg.append(avg_wghtd_r_lg)
    sd_wghtd_rs_lg.append(sd_wghtd_r_lg)
    tot_invs_lg.append(tot_inv_lg)
    tot_loans_invs_lg.append(tot_loans_inv_lg)
    recalls_lg.append(recall_lg)
    avg_wghtd_r_rfc, sd_wghtd_r_rfc, tot_inv_rfc, tot_loans_inv_rfc, recall_rfc = results_clf(y_prob_rfc, thresh, Xc_test, yc_test, y_return_test, False)
    avg_wghtd_rs_rfc.append(avg_wghtd_r_rfc)
    sd_wghtd_rs_rfc.append(sd_wghtd_r_rfc)
    tot_invs_rfc.append(tot_inv_rfc)
    tot_loans_invs_rfc.append(tot_loans_inv_rfc)
    recalls_rfc.append(recall_rfc)
    avg_wghtd_r_knn, sd_wghtd_r_knn, tot_inv_knn, tot_loans_inv_knn, recall_knn = results_clf(y_prob_knn, thresh, Xc_test, yc_test, y_return_test, False)
    avg_wghtd_rs_knn.append(avg_wghtd_r_knn)
    sd_wghtd_rs_knn.append(sd_wghtd_r_knn)
    tot_invs_knn.append(tot_inv_knn)
    tot_loans_invs_knn.append(tot_loans_inv_knn)
    recalls_knn.append(recall_knn)
    
clf_recalls_df = pd.DataFrame({
    'Recall LG': recalls_lg, 'Recall RFC': recalls_rfc, 'Recall KNN': recalls_knn
})
clf_recalls_df.index = thresholds

clf_avg_returns_df = pd.DataFrame({
    'Avg Weighted Return LG': avg_wghtd_rs_lg, 'SD Returns LG': sd_wghtd_rs_lg,
    'Amount Invested LG': tot_invs_lg, 'Loans invested LG': tot_loans_invs_lg,
    'Avg Weighted Return RFC': avg_wghtd_rs_rfc, 'SD Returns RFC': sd_wghtd_rs_rfc,
    'Amount Invested RFC': tot_invs_rfc, 'Loans invested RFC': tot_loans_invs_rfc,
    'Avg Weighted Return KNN': avg_wghtd_rs_knn, 'SD Returns KNN': sd_wghtd_rs_knn,
    'Amount Invested KNN': tot_invs_knn, 'Loans invested KNN': tot_loans_invs_knn
})
clf_avg_returns_df.index = thresholds

# # # Regression/Classification Comparison # # #

sorted_lr = [y for _, y in sorted(zip(y_pred_lr, yr_test), reverse=True)]
sorted_gbr = [y for _, y in sorted(zip(y_pred_gbr, yr_test), reverse=True)]
sorted_lg = [y for _, y in sorted(zip(y_prob_lg, y_return_test))]
sorted_rfc = [y for _, y in sorted(zip(y_prob_rfc, y_return_test))]
sorted_knn = [y for _, y in sorted(zip(y_prob_knn, y_return_test))]
full_pred_df = pd.DataFrame({'LR': sorted_lr, 'GBR': sorted_gbr, 'LG': sorted_lg, 'RFC': sorted_rfc, 'KNN': sorted_knn})

top_loans = [5000] + np.arange(10000, 100001, 10000).tolist()
df_top = pd.DataFrame(columns=['LR', 'GBR', 'LG', 'RFC', 'KNN'])
for top in range(len(top_loans)):
    df_top.loc[top, 'LR'] = (round(np.mean(full_pred_df.loc[range(0, top_loans[top]+1), 'LR']), 3),
                             round(np.std(full_pred_df.loc[range(0, top_loans[top]+1), 'LR']), 3))
    df_top.loc[top, 'GBR'] = (round(np.mean(full_pred_df.loc[range(0, top_loans[top]+1), 'GBR']), 3),
                              round(np.std(full_pred_df.loc[range(0, top_loans[top]+1), 'GBR']), 3))
    df_top.loc[top, 'LG'] = (round(np.mean(full_pred_df.loc[range(0, top_loans[top]+1), 'LG']), 3),
                             round(np.std(full_pred_df.loc[range(0, top_loans[top]+1), 'LG']), 3))
    df_top.loc[top, 'RFC'] = (round(np.mean(full_pred_df.loc[range(0, top_loans[top]+1), 'RFC']), 3),
                              round(np.std(full_pred_df.loc[range(0, top_loans[top]+1), 'RFC']), 3))
    df_top.loc[top, 'KNN'] = (round(np.mean(full_pred_df.loc[range(0, top_loans[top]+1), 'KNN']), 3),
                              round(np.std(full_pred_df.loc[range(0, top_loans[top]+1), 'KNN']), 3))
df_top.index = top_loans

# # # Investigating Classification results # # #

plot_roc_curve(yc_test, [y_prob_lg, y_prob_rfc, y_prob_knn], ['b', 'c', 'g'], ['Logistic Regression', 'Random Forest', 'KNN'])
plt.show()
# plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'roc_curve.png')
print()
plt.plot(thresholds, avg_wghtd_rs_lg, color='c', label='Logistic Regression')
plt.plot(thresholds, avg_wghtd_rs_rfc, color='b', label='Random Forest')
plt.plot(thresholds, avg_wghtd_rs_knn, color='g', label='KNN')
plt.title('Average weighted return per threshold')
plt.xlabel('Threshold')
plt.legend(loc="lower left")
plt.show()
# plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'avg_returns_weight.png')

print()
plt.plot(thresholds, sd_wghtd_rs_lg, color='c', label='Logistic Regression')
plt.plot(thresholds, sd_wghtd_rs_rfc, color='b', label='Random Forest')
plt.plot(thresholds, sd_wghtd_rs_knn, color='g', label='KNN')
plt.title('Standard Deviation of the returns per threshold')
plt.xlabel('Threshold')
plt.legend(loc="lower left")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'sd_returns.png')

num = len(thresholds)
clf_inv_in_data = pd.DataFrame(zip(thresholds.tolist()*num, ['Logistic Regression']*num + ['Random Forest']*num+['KNN']*num,
                                   tot_invs_lg+tot_invs_rfc+tot_invs_knn), columns=['threshold', 'kind', 'data'])
plt.figure(figsize=(10, 6))
sns.barplot(x='threshold', hue='kind', y='data', data=clf_inv_in_data)
plt.title('Total amount invested')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'total_amount_invested.png')

clf_loans_in_data = pd.DataFrame(zip(thresholds.tolist()*num, ['Logistic Regression']*num+['Random Forest']*num+['KNN']*num,
                                     tot_loans_invs_lg+tot_loans_invs_rfc+tot_loans_invs_knn), columns=['threshold', 'kind', 'data'])
plt.figure(figsize=(10, 6))
sns.barplot(x='threshold', hue='kind', y='data', data=clf_loans_in_data)
plt.title('Total loans invested')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'total_loans_invested.png')

# # # Results to CSV # # #
results_directory = get_project_folder() / 'results'
reg_avg_returns_df.to_csv(f'{results_directory}/reg_results_stage4.csv')
clf_avg_returns_df.to_csv(f'{results_directory}/clf_results_stage4.csv')
clf_recalls_df.to_csv(f'{results_directory}/clf_recalls_stage4.csv')
df_top.to_csv(f'{results_directory}/comparison_reg_clf_stage4.csv')
