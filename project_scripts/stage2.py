import pandas as pd
from pathlib import Path
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import plotnine as pt

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'  # directory of data files

# # # Concat 2018-2019 files into two files (they will be saved under 'raw_files') # # #

def concat_files_to_one(folder, year, args):
    merged_df = pd.DataFrame()
    for i in range(4):
        filename = files_directory / folder / f'LoanStats_2016Q{i + 1}.csv'
        df = pd.read_csv(filename, **args)
        merged_df = merged_df.append(df)
    new_name = files_directory / f'loan_status_{year}_full.csv'
    merged_df.reset_index(drop=True, inplace=True)
    merged_df.to_csv(new_name)

LOAN2018 = 'loan-data-apr-2018-snap'
LOAN2019 = 'loan-data-jul-2019-snap'
read_csv_args = {'header': 1, 'skipfooter': 2}

# concat_files_to_one(LOAN2018, '2018', read_csv_args)     # concat 2018 files
# concat_files_to_one(LOAN2019, '2019', read_csv_args)     # concat 2019 files

# # # Read the two new files and reordering by 'id' # # #

full_2018 = pd.read_csv(files_directory / 'loan_status_2018_full.csv', index_col=0).reset_index(drop=True)
full_2019 = pd.read_csv(files_directory / 'loan_status_2019_full.csv', index_col=0).reset_index(drop=True)
full_2018 = full_2018.sort_values('id').reset_index(drop=True)
full_2019 = full_2019.sort_values('id').reset_index(drop=True)

# # # Calculating the expected realized return annualized for each loan # # #

def ann_exp_return(f, p, m, t):  # realized return calculation for each loan
    rate = pow(1+0.02, 1/12) - 1
    return (12/t) * (1/f) * (((p/m) * ((1-pow(1+rate, m))/(1-(1+rate)))) * pow(1+rate, t-m)-f) if (m!=0) else ((p-f)/f)

return_2019 = full_2019.loc[:, ['id', 'loan_status', 'grade', 'term', 'int_rate', 'issue_d', 'last_pymnt_d', 'funded_amnt',
                                'total_pymnt', 'collection_recovery_fee']]

return_2019['term'] = return_2019['term'].str[1:3].astype(int)             # changing feature to numeric format
return_2019['issue_d'] = pd.to_datetime(return_2019['issue_d'])            # changing feature to datetime format
return_2019['last_pymnt_d'] = pd.to_datetime(return_2019['last_pymnt_d'])  # changing feature to datetime format
return_2019['full_amnt_rec'] = return_2019['total_pymnt'] + return_2019['collection_recovery_fee']

# calculating actual loan term
return_2019['actual_term'] = np.round(((return_2019['last_pymnt_d'] - return_2019['issue_d']) / np.timedelta64(1, 'M')))

# calculating the annual expected return for each loan with the formula above
return_2019['ann_exp_return'] = return_2019.apply(lambda row: ann_exp_return(row.funded_amnt, row.full_amnt_rec,
                                                                             row.actual_term, row.term), axis=1)
print(len(return_2019[return_2019['actual_term'].isna()].index))

# keeping the id of each loan and its return for later
return_2019 = return_2019.loc[:, ['id', 'ann_exp_return', 'loan_status', 'grade', 'term', 'int_rate', 'actual_term']]

return_2019_for_graph = return_2019.sort_values('grade').reset_index(drop=True)
return_2019_for_graph = return_2019_for_graph[(return_2019_for_graph['loan_status'] == 'Fully Paid') |
                                              (return_2019_for_graph['loan_status'] == 'Charged Off')]
return_2019_for_graph = return_2019_for_graph[return_2019_for_graph['actual_term'].notna()]
return_2019_for_graph = return_2019_for_graph[return_2019_for_graph['term'] == 36]
# print(return_2019_for_graph.groupby('grade').agg('mean')['ann_exp_return'])
(
        pt.ggplot(return_2019_for_graph, pt.aes('grade', 'ann_exp_return')) +
        pt.geom_boxplot() +
        pt.ylim(-0.3, 0.3) +
        pt.coord_flip() +
        pt.facet_wrap('loan_status') +
        pt.geom_hline(yintercept=0, color='red') +
        pt.ggtitle('Distribution of returns by grades')
)
# # # First filtering # # #

full_2018_names = full_2018.columns
full_2019_names = full_2019.columns

# checking if we have an extra feature in either 2018 or 2019
print("Columns appearing in 2018 and not in 2019:")
for i in range(len(full_2018_names)):
    if full_2018_names[i] not in full_2019_names:
        print(full_2018_names[i])
print("Columns appearing in 2019 and not in 2018:")
for i in range(len(full_2019_names)):
    if full_2019_names[i] not in full_2018_names:
        print(full_2019_names[i])

print(full_2018['disbursement_method'].value_counts())   # checking if the extra feature in 2018 is relevant

fig, ax = plt.subplots(figsize=(10, 8))
x = [str(value) for value in sorted(full_2019['term'].unique())]
y0 = full_2019[(full_2019['loan_status'] == 'Fully Paid') | (full_2019['loan_status'] == 'Charged Off')].groupby('term').size().tolist()
y1 = full_2019[(full_2019['loan_status'] != 'Fully Paid') & (full_2019['loan_status'] != 'Charged Off')].groupby('term').size().tolist()
ax.bar(x, y0, color='b', label='Fully Paid/ Charged Off')
ax.bar(x, y1, color='c', bottom=y0, label='Others')
plt.legend()
plt.title('Distinction between the two options of loan term', fontsize=20)
plt.xlabel('Term')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'term_distribution.png')

# deleting all loans of 60 months and all those who are not fully paid or charged off in 2019
relevant_2019 = full_2019[(full_2019['term'] != ' 60 months')]
relevant_2019 = relevant_2019[(relevant_2019['loan_status'] == 'Fully Paid') |
                              (relevant_2019['loan_status'] == 'Charged Off')]
relevant_2019 = relevant_2019[relevant_2019['last_pymnt_d'].notna()]

# # # Cleaning featured with too many NaNs # # #

# checking the percentage of missing values in all features in 2019 file
prop_nas = []
print("2019 files:")
for col in relevant_2019.columns:
    print("Column: %s --> %.3f%% NaNs" % (col, (relevant_2019[col].isna().mean()) * 100))
    prop_nas.append(relevant_2019[col].isna().mean())

fig, ax = plt.subplots(figsize=(10, 8))
plt.scatter(relevant_2019.columns, prop_nas, s=15, color='c')
plt.axhline(y=0.45, color='r', linestyle='-')
plt.text(len(relevant_2019.columns)/2, 0.45, 'Threshold=0.45', fontsize=15, va='center', ha='center', backgroundcolor='w')
plt.gca().axes.xaxis.set_ticklabels([])
plt.title("Proportion of missing values by features", fontsize=20)
plt.xlabel("Data Features")
plt.ylabel("Proportion of missing values")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'features_nas_prop.png')

# dropping feature with too many NaNs from both 2019 and 2018 file as well as the extra feature in 2018.
to_drop_nas = [col for col in relevant_2019.columns if relevant_2019[col].isna().mean() >= 0.45]
filtered_2019 = relevant_2019.drop(to_drop_nas, axis=1)
filtered_2018 = full_2018.drop(to_drop_nas + ['disbursement_method'], axis=1)
filtered_2018 = filtered_2018[filtered_2018['id'].isin(filtered_2019['id'])]

# # # Cleaning featured with leakage data (too many differences between 2018 and 2019 # # #

# comparing the differences between the 2018 and 2019 snapshot to check for leakage data
is_diff_bool = (filtered_2019 != filtered_2018)
df_only_diff = is_diff_bool.loc[:, is_diff_bool.any()]
diff_cols = df_only_diff.columns
same_cols = [col for col in filtered_2019.columns if col not in diff_cols]

full_merged = filtered_2019.merge(filtered_2018, how='left', on=same_cols, suffixes=['_2018', '_2019'])
total_diff = []
for col in filtered_2019.columns:
    if col in same_cols:
        pass
    else:
        print("\nColumn: %s" % col)
        col18, col19 = f'{col}_2018', f'{col}_2019'
        print("Na's in 2018 %d" % full_merged[col18].isna().sum())
        print("Na's in 2019 %d" % full_merged[col19].isna().sum())
        print("%.3f%% different values between 2018 and 2019" % ((df_only_diff[col].mean())*100))
        total_diff.append((df_only_diff[col].mean()))
print(sorted(total_diff, reverse=True))

fig, ax = plt.subplots(figsize=(10, 8))
plt.scatter(diff_cols, total_diff, s=15, color='c')
plt.axhline(y=0.4, color='r', linestyle='-')
plt.text(len(diff_cols)/2, 0.4, 'Threshold=0.4', fontsize=15, va='center', ha='center', backgroundcolor='w')
plt.gca().axes.xaxis.set_ticklabels([])
plt.title("Proportion of leakage data in each feature", fontsize=20)
plt.xlabel("Data Features")
plt.ylabel("Proportion of differences between 2018-2019")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'leakage.png')

# dropping all leakage variables
to_drop_leakage = [col for col in filtered_2019.columns if col not in same_cols and df_only_diff[col].mean() > 0.40] + \
                  ['delinq_2yrs', 'collections_12_mths_ex_med', 'open_il_12m', 'open_il_24m',
                   'open_rv_12m', 'open_rv_24m', 'open_acc_6m', 'open_act_il', 'inq_last_12m', 'inq_last_6mths',
                   'acc_open_past_24mths', 'num_tl_120dpd_2m', 'num_tl_30dpd', 'num_tl_90g_dpd_24m',
                   'num_tl_op_past_12m', 'chargeoff_within_12_mths']
filtered2_2019 = filtered_2019.drop(to_drop_leakage, axis=1)

# # # Cleaning featured irrelevant content (either same values in the variable or simply not useful) # # #

# checking the proportion of unique values in the remaining features
total_rows = len(filtered2_2019.index)
max_value_perc_list = []
for col in filtered2_2019.columns:
    max_value = filtered2_2019[col].mode()[0]
    max_col_df = filtered2_2019[filtered_2019[col] == max_value]
    max_value_perc = len(max_col_df.index) / total_rows
    print("Column: %s: max value is %s with %.3f" % (col, max_value, max_value_perc))
    max_value_perc_list.append(max_value_perc)

fig, ax = plt.subplots(figsize=(10, 8))
plt.scatter(filtered2_2019.columns, max_value_perc_list, s=15, color='c')
plt.axhline(y=0.7, color='r', linestyle='-')
plt.text(len(filtered2_2019.columns)/2, 0.7, 'Threshold=0.7', fontsize=15, va='center', ha='center', backgroundcolor='w')
plt.gca().axes.xaxis.set_ticklabels([])
plt.title("Proportion of the mode in each feature", fontsize=20)
plt.xlabel("Data Features")
plt.ylabel("Total same values")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'unique_values.png')

# dropping the feature with many unique values as well as uninformative ones.
to_drop_same_values = [col for col in filtered2_2019.columns if (
        len((filtered2_2019[filtered_2019[col] == filtered2_2019[col].mode()[0]]).index) / total_rows) >= 0.7]
to_drop_same_values.remove('term')
to_drop_irrelevant = to_drop_same_values + ['funded_amnt', 'funded_amnt_inv', 'verification_status', 'url',
                                            'zip_code', 'earliest_cr_line']
filtered3_2019 = filtered2_2019.drop(to_drop_irrelevant, axis=1)

# # # Cleaning featured highly correlated with others or not at all with the return # # #

# Joining the returns to the our current table
filtered3_2019_with_return = filtered3_2019.merge(return_2019.iloc[:, :2], how='left', on='id')
cols = filtered3_2019_with_return.columns

# Checking correlation by features that resembles each other (in groups)
df_to_corr1 = filtered3_2019_with_return.loc[:, [col for col in cols if col.startswith('num')] + ['ann_exp_return']]
fig0, ax0 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr1.corr(method='pearson'), annot=True)
plt.title("Pearson Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr11.png')
fig1, ax1 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr1.corr(method='spearman'), annot=True)
plt.title("Spearman Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr12.png')

df_to_corr2 = filtered3_2019_with_return.loc[:, [col for col in cols if col.startswith('mo')] + ['ann_exp_return']]
fig2, ax2 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr2.corr(method='pearson'), annot=True)
plt.title("Pearson Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr21.png')
fig3, ax3 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr2.corr(method='spearman'), annot=True)
plt.title("Spearman Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr22.png')

df_to_corr3 = filtered3_2019_with_return.loc[:, ['revol_bal', 'revol_util', 'il_util', 'bc_util', 'bc_open_to_buy'] +
                                                ['ann_exp_return']]
df_to_corr3['revol_util'] = (df_to_corr3['revol_util'].str[:-1].astype(float))/100
fig4, ax4 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr3.corr(method='pearson'), annot=True)
plt.title("Pearson Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr31.png')
fig5, ax5 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr3.corr(method='spearman'), annot=True)
plt.title("Spearman Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr32.png')

df_to_corr4 = filtered3_2019_with_return.loc[:, ['purpose', 'title']]
print(df_to_corr4['purpose'].value_counts())
print(df_to_corr4['title'].value_counts())

df_to_corr5 = filtered3_2019_with_return.loc[:, ['total_acc', 'open_acc', 'total_bal_il'] + ['ann_exp_return']]
fig6, ax6 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr5.corr(method='pearson'), annot=True)
plt.title("Pearson Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr41.png')
fig7, ax7 = plt.subplots(figsize=(16, 12))
sns.heatmap(df_to_corr5.corr(method='spearman'), annot=True)
plt.title("Spearman Correlation", fontsize=20)
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / f'corr42.png')

to_drop_corr = [col for col in cols if col.startswith('num')] + \
               ['revol_util', 'il_util', 'bc_util', 'title', 'total_acc', 'total_bal_il']
to_not_drop_corr = ['num_bc_stats', 'mort_acc', 'mo_sin_rcnt_tl', 'open_acc']
to_drop_corr = [x for x in to_drop_corr if x not in to_not_drop_corr]

filtered4_2019 = filtered3_2019.drop(to_drop_corr, axis=1)
filtered4_2019_with_return = filtered3_2019_with_return.drop(to_drop_corr, axis=1)

filtered4_2019_with_return['int_rate'] = filtered4_2019_with_return.apply(lambda row: float(row.int_rate[:-1])/100, axis=1)
# filtered4_2019_names = filtered4_2019.columns.tolist()
corr_pearson, corr_spearman, cols_to_corr = [], [], []
for col in filtered4_2019_with_return.columns:
    if (filtered4_2019_with_return.dtypes.loc[col] == 'object') | (col == 'id') | (col == 'ann_exp_return'):
        pass
    else:
        cols_to_corr.append(col)
        pearson_corr = filtered4_2019_with_return.loc[:, [col, 'ann_exp_return']].corr('pearson').iloc[0, 1]
        corr_pearson.append(pearson_corr)
        spearman_corr = filtered4_2019_with_return.loc[:, [col, 'ann_exp_return']].corr('spearman').iloc[0, 1]
        corr_spearman.append(spearman_corr)

df_corr = pd.DataFrame({'Features': cols_to_corr, 'Pearson': corr_pearson, 'Spearman': corr_spearman})
print(df_corr.sort_values('Pearson', ascending=True).head(5))
print(df_corr.sort_values('Pearson', ascending=False).head(5))
print(df_corr.sort_values('Spearman', ascending=True).head(5))
print(df_corr.sort_values('Spearman', ascending=False).head(5))
fig, ax = plt.subplots(figsize=(10, 8))
plt.scatter(cols_to_corr, corr_pearson, s=15, color='c')
plt.gca().axes.xaxis.set_ticklabels([])
plt.title("Pearson Correlation Coordinators")
plt.xlabel("Data Features")
plt.ylabel("Correlation Coordinator")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / f'final_corr_pearson.png')

fig1, ax1 = plt.subplots(figsize=(10, 8))
plt.scatter(cols_to_corr, corr_spearman, s=15, color='c')
plt.gca().axes.xaxis.set_ticklabels([])
plt.title("Spearman Correlation Coordinators")
plt.xlabel("Data Features")
plt.ylabel("Correlation Coordinator")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / f'final_corr_spearman.png')

filtered4_2019_with_return.to_csv(files_directory / 'stage2_final.csv')
return_2019.to_csv(files_directory / 'returns_2019.csv')
