import pandas as pd
import re
from pathlib import Path
from sklearn.preprocessing import StandardScaler
from sklearn.impute import KNNImputer
import pickle
import argparse

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'
script_directory = get_project_folder() / 'project_scripts'

file_to_predict = f'{files_directory}/loan_status_2019_full.csv'
model_gbc = f'{script_directory}/finalized_gbc.sav'
loaded_gbc = pickle.load(open(model_gbc, 'rb'))

relevant_columns = ['loan_amnt', 'int_rate', 'annual_inc', 'dti', 'fico_range_low', 'open_acc', 'max_bal_bc',
                    'all_util', 'total_rev_hi_lim', 'inq_fi', 'total_cu_tl', 'avg_cur_bal', 'bc_open_to_buy',
                    'mo_sin_old_rev_tl_op', 'mo_sin_rcnt_rev_tl_op', 'mo_sin_rcnt_tl', 'mort_acc',
                    'mths_since_recent_bc', 'pct_tl_nvr_dlq', 'percent_bc_gt_75', 'total_bal_ex_mort']

def cleaning(df: pd.DataFrame) -> pd.DataFrame:
    return df.loc[:, relevant_columns]


def preprocessing(df: pd.DataFrame) -> pd.DataFrame:
    df['int_rate'] = df['int_rate'].apply(lambda row: float(row[:-1])/100)
    df['emp_length'] = df['emp_length'].apply(lambda row: ''.join(re.findall(r'\d+', str(row))) if row == row else row)
    is_home_own = {'MORTGAGE': 0, 'RENT': 0, 'ANY': 0, 'OWN': 1}
    df = df.replace({'home_ownership': is_home_own})

    sc = StandardScaler()
    df_sc = sc.fit_transform(df)  # scaling data
    imputer = KNNImputer()  # knn imputation model
    imputer.fit(df_sc)
    arr_knn_imputed = imputer.transform(df_sc)
    df_knn_imputed = pd.DataFrame(sc.inverse_transform(arr_knn_imputed), columns=df.columns)
    return df_knn_imputed


def investment_budget(df, original_df, final_model, budget):
    loaded_model = pickle.load(open(final_model, 'rb'))
    y_prob = pd.Series(loaded_model.predict_proba(df)[:, 1])
    amnt_of_loans = original_df['loan_amount']
    y_prob.index = amnt_of_loans.index
    full_pred_df = pd.concat([y_prob, amnt_of_loans], axis=1)
    full_pred_df.columns = ['probs', 'loan_amount']
    full_pred_df.sort_values(by='probs', inplace=True)
    amount, tot_loans = 0, 0
    for row in range(len(full_pred_df.index)):
        new_loan = int(full_pred_df.iloc[row, 2])
        if (amount + new_loan) <= budget:
            amount += new_loan
            tot_loans += 1
        else:
            break

    df_budget = full_pred_df.iloc[list(range(tot_loans)), :]
    df_to_invest = original_df.loc[df_budget.index, :]
    return df_to_invest


def run(file_name, budget):
    ori_df = pd.read_csv(file_name, index_col=0)
    df = cleaning(ori_df)
    df = preprocessing(df)
    loans_to_invest = investment_budget(df, ori_df, loaded_gbc, budget)
    loans_to_invest.to_csv(f'{get_project_folder()}/Loans_to_invest.csv')

parser = argparse.ArgumentParser(description='temp')
parser.add_argument('--budget', type=int, required=True)
args = parser.parse_args()
# args = parser.parse_args('--budget 200000000'.split())
run(file_to_predict, args.budget)
