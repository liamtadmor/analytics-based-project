import pandas as pd
import numpy as np
import time
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LinearRegression, LogisticRegression, Ridge, Lasso
from sklearn.ensemble import GradientBoostingRegressor, RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score
#from sklearn.feature_selection import SequentialFeatureSelector
from sklearn.pipeline import make_pipeline
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
import numpy as np
import statsmodels.api as sm
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler


def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'

files_directory = get_project_folder() / 'raw_files'  # directory of data file


df = pd.read_csv(f'{files_directory}/stage3_after_nas.csv', index_col=0)

#df = pd.read_csv('stage3_after_nas.csv', index_col=0)
#df['loan_status'] = df['loan_status'].astype(int)

X_reg = df.drop(['loan_status', 'ann_exp_return'], axis=1)
y_reg = df['ann_exp_return']
Xr_train, Xr_test, yr_train, yr_test = train_test_split(X_reg, y_reg, test_size=0.4)

LR = LinearRegression()
LR.fit(Xr_train, yr_train)
y_pred_lr = LR.predict(Xr_test)
print('LR R-squared: %.4f' % r2_score(yr_test, y_pred_lr))
print('LR mse: %.4f' % mean_squared_error(yr_test, y_pred_lr))

GBR = GradientBoostingRegressor()
GBR.fit(Xr_train, yr_train)
y_pred_gbr = GBR.predict(Xr_test)
print('GBR R-squared: %.4f' % r2_score(yr_test, y_pred_gbr))
print('GBR mse: %.4f' % mean_squared_error(yr_test, y_pred_gbr))


#create scalarL
# scaler = preprocessing.StandardScaler()
# scaled_df = scaler.fit_transform(df)
# scaled_df = pd.DataFrame(scaled_df, columns=df.columns)
# scaled_df['ann_exp_return'].hist()

sc_X = StandardScaler()
X = df.drop(['loan_status', 'ann_exp_return'], axis=1)
y = df['ann_exp_return']
X_train, X_test, y_train, y_test =  train_test_split(X,y,test_size = 0.2, random_state= 0)

Xsc_train = pd.DataFrame(sc_X.fit_transform(X_train), columns=X_train.columns)
Xsc_test = pd.DataFrame(sc_X.transform(X_test), columns=X_test.columns)

regressor = LinearRegression()
regressor.fit(Xsc_train, y_train)#predicting the test set results
y_pred = regressor.predict(Xsc_test)

print('LR R-squared: %.4f' % r2_score(y_test, y_pred))
print('LR mse: %.4f' % mean_squared_error(y_test, y_pred))


Xsc_train['bias'] = [1]*len(Xsc_train.index)
regressor = sm.OLS(y_train, np.array(Xsc_train)).fit()
print(regressor.summary())


# X_opt = [0,1,2,3,4,5,6,7,8,9,10,12,15,19,20,22,23,24,27,28,29,30,31,33,34,35]
regressor_OLS = sm.OLS(y_train, np.array(Xsc_train)).fit()
print(regressor_OLS.summary())
np.argmax(regressor_OLS.pvalues)

X_opt = list(range(36))
col_opt = Xsc_train.columns.tolist()
for i in range(20):
    regressor_OLS = sm.OLS(y_train, np.array(Xsc_train.iloc[:, X_opt])).fit()
    max_pvalue = int(np.argmax(regressor_OLS.pvalues))
    X_opt.pop(max_pvalue)
    col_opt.pop(max_pvalue)

print(regressor_OLS.summary())

for col in Xsc_train.columns:
    if col not in col_opt:
        print(col)

X_opt.remove(35)
regressor = LinearRegression()
regressor.fit(Xsc_train.iloc[:, X_opt], y_train) # predicting the test set results
y_pred = regressor.predict(Xsc_test.iloc[:, X_opt])

print('LR R-squared: %.4f' % r2_score(y_test, y_pred))
print('LR mse: %.4f' % mean_squared_error(y_test, y_pred))


GBR=GradientBoostingRegressor()
search_grid={'n_estimators':[500,1000,2000],
             'learning_rate':[.001,0.01,.1],
             'max_depth':[1,2,4],
             'subsample':[.5,.75,1],
             'random_state':[1,20]}
search=GridSearchCV(estimator=GBR,param_grid=search_grid,scoring='neg_mean_squared_error',n_jobs=1,cv=5)

search.fit(X_train,y_train)
search.best_params_
search.best_score_


GBR2 = GradientBoostingRegressor(n_estimators=500, learning_rate=0.01, subsample=.5, max_depth=1, random_state=1)
y_pred_gbr2 = GBR2.predict(X_test)
print('LR R-squared: %.4f' % r2_score(y_test, y_pred_gbr2))
print('LR mse: %.4f' % mean_squared_error(y_test, y_pred_gbr2))