import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_auc_score, roc_curve

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'  # directory of data file
results_directory = get_project_folder() / 'results'

df = pd.read_csv(f'{files_directory}/stage4_after_anomalies.csv', index_col=0)
df['loan_status'] = df['loan_status'].astype(int)

relevant_columns = ['loan_amnt', 'int_rate', 'annual_inc', 'dti', 'fico_range_low', 'open_acc', 'max_bal_bc',
                    'all_util', 'total_rev_hi_lim', 'inq_fi', 'total_cu_tl', 'avg_cur_bal', 'bc_open_to_buy',
                    'mo_sin_old_rev_tl_op', 'mo_sin_rcnt_rev_tl_op', 'mo_sin_rcnt_tl', 'mort_acc',
                    'mths_since_recent_bc', 'pct_tl_nvr_dlq', 'percent_bc_gt_75', 'total_bal_ex_mort',
                    'loan_status', 'ann_exp_return']
df = df[relevant_columns]

X_clf = df.drop(['loan_status', 'ann_exp_return'], axis=1)
y_clf = df['loan_status']
y_return = df['ann_exp_return']
Xc_train, Xc_test, yc_train, yc_test, y_return_train, y_return_test = \
    train_test_split(X_clf, y_clf, y_return, test_size=0.4, stratify=y_clf)

scaler_clf = StandardScaler()
Xc_train_sc = pd.DataFrame(scaler_clf.fit_transform(Xc_train))
Xc_train_sc.columns = Xc_train.columns
Xc_test_sc = pd.DataFrame(scaler_clf.transform(Xc_test))
Xc_test_sc.columns, Xc_test_sc.index = Xc_test.columns, Xc_test.index

def train_clf(X_train, y_train, X_test, model):
    model.fit(X_train, y_train)
    y_prob = model.predict_proba(X_test)[:, 1]
    return y_prob

def investment(y_prob, yr, amnt_of_loans, budget):
    y_prob_series = pd.Series(y_prob)
    y_prob_series.index = yr.index
    full_pred_df = pd.concat([y_prob_series, yr, amnt_of_loans], axis=1)
    full_pred_df.columns = ['probs', 'ann_exp_return', 'loan_amount']
    full_pred_df.sort_values(by='probs', inplace=True)
    amount, tot_loans = 0, 0
    for row in range(len(full_pred_df.index)):
        new_loan = int(full_pred_df.iloc[row, 2])
        if (amount + new_loan) <= budget:
            amount += new_loan
            tot_loans += 1
        else:
            break

    df_budget = full_pred_df.iloc[list(range(tot_loans)), :]
    tot_inv = np.sum(df_budget['loan_amount'])
    tot_loans_inv = len(df_budget['loan_amount'])
    avg_wght_return = np.sum((df_budget['loan_amount'] / tot_inv) * df_budget['ann_exp_return'])
    sd_wght_return = np.sqrt(np.sum(pow((df_budget['ann_exp_return'] - avg_wght_return), 2)) / len(df_budget['ann_exp_return']))
    max_return = df_budget['ann_exp_return'].max()
    return avg_wght_return, sd_wght_return, max_return, tot_loans_inv, tot_inv

# # # Comparison with grades # # #

df_grades = pd.read_csv(f'{files_directory}/df_grades.csv', index_col=0)

grades_returns = df_grades.groupby('grade').agg(['mean', 'std', 'max'])['ann_exp_return']
grades_status_loan = (df_grades.groupby('grade').agg('sum')['loan_status'])/\
                     (df_grades.groupby('grade').agg('count')['loan_status'])
grades_investment_prop = (df_grades.groupby('grade').agg('sum')['loan_amnt'])/(np.sum(df_grades['loan_amnt']))
summary_grades = pd.concat([grades_returns, grades_status_loan, grades_investment_prop], axis=1)
summary_grades.columns = ['avg_exp_return', 'sd_exp_return', 'max_exp_return', 'prob_default', 'prop_invested']

amnt_loans = Xc_test['loan_amnt']

GBC = GradientBoostingClassifier()
y_prob_gbc = train_clf(Xc_train_sc, yc_train, Xc_test_sc, GBC)

GRADE_A = summary_grades.loc['A', 'prop_invested']
GRADE_B = summary_grades.loc['B', 'prop_invested']
GRADE_C = summary_grades.loc['C', 'prop_invested']
GRADE_D = summary_grades.loc['D', 'prop_invested']
GRADE_E = summary_grades.loc['E', 'prop_invested']
GRADE_F = summary_grades.loc['F', 'prop_invested']
GRADE_G = summary_grades.loc['G', 'prop_invested']

total_amount = sum(amnt_loans)
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_A*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_B*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_C*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_D*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_E*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_F*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_G*total_amount))


# # # Graphs avg returns # # #
loan_amounts = Xc_test.loc[:, 'loan_amnt']
max_investment = np.sum(loan_amounts)
budgets = list(np.arange(100000000, max_investment, 100000000))
avg_wghtd_rs_gbc, sd_wghtd_rs_gbc, max_rs_gbc, tot_loans_invs_gbc, tot_invs_gbc = [], [], [], [], []
for bud in budgets:
    avg_wghtd_r_gbc, sd_wghtd_r_gbc, max_r_gbc, tot_loans_inv_gbc, tot_inv_gbc = investment(y_prob_gbc, y_return_test, loan_amounts, bud)
    avg_wghtd_rs_gbc.append(avg_wghtd_r_gbc)
    sd_wghtd_rs_gbc.append(sd_wghtd_r_gbc)
    max_rs_gbc.append(max_r_gbc)
    tot_invs_gbc.append(tot_inv_gbc)
    tot_loans_invs_gbc.append(tot_loans_inv_gbc)

ind_max_gbc = avg_wghtd_rs_gbc.index(max(avg_wghtd_rs_gbc))
plt.plot(budgets, avg_wghtd_rs_gbc, color='g', label='Gradient Boosting Classifier')
plt.plot(budgets[ind_max_gbc], avg_wghtd_rs_gbc[ind_max_gbc], color='g', marker=".", markersize=14)
plt.title('Average weighted return per portfolio')
plt.xlabel('Portfolios')
plt.legend(loc="lower left")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage6_avg_returns_weight.png')

print()
plt.plot(budgets, sd_wghtd_rs_gbc, color='g', label='Gradient Boosting Classifier')
plt.title('Standard Deviation of the returns per portfolio')
plt.xlabel('Portfolios')
plt.legend(loc="lower right")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage6_sd_returns.png')

def plot_roc_curve(y_test, y_probs, colors, labels):
    plt.figure(figsize=(10, 6))
    plt.plot([0, 1], [0, 1], 'r--')
    for i in range(len(y_probs)):
        fpr, tpr, threshs = roc_curve(y_test, y_probs[i])
        plt.plot(fpr, tpr, colors[i], label=labels[i])
        plt.text(0.6, 0.2-(i*4/100), f'AUC {labels[i]}: {round(roc_auc_score(y_test, y_probs[i]), 2)}')
    plt.legend()
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

plot_roc_curve(yc_test, [y_prob_gbc], ['g'], ['Gradient Boosting Classifer'])
plt.title('Our Model VS Random Classifier')
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage6_roc_curve.png')

