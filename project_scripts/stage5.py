import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, ParameterGrid
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import recall_score, roc_auc_score, roc_curve
from sklearn.feature_selection import SelectKBest, f_classif
import pickle

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'  # directory of data file
results_directory = get_project_folder() / 'results'

df = pd.read_csv(f'{files_directory}/stage4_after_anomalies.csv', index_col=0)
df['loan_status'] = df['loan_status'].astype(int)

X_clf = df.drop(['loan_status', 'ann_exp_return'], axis=1)
y_clf = df['loan_status']
y_return = df['ann_exp_return']
Xc_train, Xc_test, yc_train, yc_test, y_return_train, y_return_test = \
    train_test_split(X_clf, y_clf, y_return, test_size=0.4, stratify=y_clf)

scaler_clf = StandardScaler()
Xc_train_sc = pd.DataFrame(scaler_clf.fit_transform(Xc_train))
Xc_train_sc.columns = Xc_train.columns
Xc_test_sc = pd.DataFrame(scaler_clf.transform(Xc_test))
Xc_test_sc.columns, Xc_test_sc.index = Xc_test.columns, Xc_test.index

def train_clf(X_train, y_train, X_test, model):
    model.fit(X_train, y_train)
    y_prob = model.predict_proba(X_test)[:, 1]
    return y_prob

def results_clf(model, y_prob, X_test, y_test, y_return, threshold, print_results=False):
    y_pred = (y_prob > threshold)
    recall = recall_score(y_test, y_pred)
    auc_score = roc_auc_score(y_test, y_prob)
    if print_results:
        print(f'Model {model}:')
        print(f'Recall --> {round(recall, 3)}')
        print(f'AUC score --> {round(auc_score, 3)}')
    returns_of_loans_to_inv = y_return[y_pred == 0]
    amnt_inv = X_test[y_pred == 0].iloc[:, 0]
    tot_inv = np.sum(amnt_inv)
    tot_loans_inv = len(amnt_inv)
    avg_wght_return = np.sum((amnt_inv/tot_inv) * returns_of_loans_to_inv)
    sd_wght_return = np.sqrt(np.sum(pow((returns_of_loans_to_inv-avg_wght_return), 2))/len(returns_of_loans_to_inv))
    return {'Model': model, 'Average Return': avg_wght_return, 'SD return': sd_wght_return,
            'Total Amount Invested': tot_inv, 'Total Loans Invested': tot_loans_inv, 'Recall': recall, 'AUC': auc_score}

# # # Advanced Feature selection # # #
fst, pvalue = f_classif(X_clf, y_clf)
f_classif_df = pd.DataFrame({'Columns': X_clf.columns.tolist(), 'f-statistic': fst, 'p-value': pvalue})
f_classif_df.to_csv(f'{results_directory}/stage5_anova_results.csv')

def feature_selection_Kbest(model, n_features, X, y, Xtrain, ytrain, Xtest, ytest, yr, Xtrain_sc, Xtest_sc, scaled=False):
    print(f'{model} is starting')
    result = pd.DataFrame(columns=['Model', 'Average Return', 'SD return', 'Total Amount Invested',
                                   'Total Loans Invested', 'Recall', 'AUC'])
    for n in n_features:
        selector = SelectKBest(f_classif, k=n)
        selector.fit(X, y)
        best_cols = X.columns[selector.get_support()]
        if scaled:
            y_prob = train_clf(Xtrain_sc.loc[:, best_cols], ytrain, Xtest_sc.loc[:, best_cols], model)
        else:
            y_prob = train_clf(Xtrain.loc[:, best_cols], ytrain, Xtest.loc[:, best_cols], model)
        res = results_clf(model, y_prob, Xtest, ytest, yr, threshold=0.2)
        res['n_features'] = n
        result = result.append(res, ignore_index=True)
        print('Iteration %d done out of %d' % (n_features.index(n) + 1, len(n_features)))
    print(f'{model} is done')
    return result

n_features = list(np.arange(10, 27, 1))
featuring_results_lg = feature_selection_Kbest(LogisticRegression(), n_features, X_clf, y_clf, Xc_train, yc_train,
                                               Xc_test, yc_test, y_return_test, Xc_train_sc, Xc_test_sc, scaled=True)
featuring_results_rfc = feature_selection_Kbest(RandomForestClassifier(), n_features, X_clf, y_clf, Xc_train, yc_train,
                                               Xc_test, yc_test, y_return_test, Xc_train_sc, Xc_test_sc, scaled=False)
featuring_results_gbc = feature_selection_Kbest(GradientBoostingClassifier(), n_features, X_clf, y_clf, Xc_train, yc_train,
                                               Xc_test, yc_test, y_return_test, Xc_train_sc, Xc_test_sc, scaled=False)
featuring_results = pd.concat([featuring_results_lg, featuring_results_rfc, featuring_results_gbc])
featuring_results.to_csv(f'{results_directory}/featuring_results.csv')

# choosing to continue with the 15 best features
selector = SelectKBest(f_classif, k=25)
selector.fit(X_clf, y_clf)
best_cols = list(X_clf.columns[selector.get_support()])

# NA's verification
original_df = pd.read_csv(f'{files_directory}/loan_status_2019_full.csv', index_col=0)
original_df = original_df.loc[:, best_cols]
prop_nas = []
for col in original_df.columns:
    print("Column: %s --> %.2f%% NaNs" % (col, (original_df[col].isna().mean()) * 100))
    prop_nas.append(original_df[col].isna().mean())
prop_nas_df = pd.DataFrame({'Columns': original_df.columns.tolist(), 'Proportion of missing values': prop_nas})
prop_nas_df.to_csv(f'{results_directory}/stage5_prop_nas.csv')

to_remove = ['emp_length', 'mths_since_rcnt_il', 'mo_sin_old_il_acct', 'mths_since_recent_inq']
for x in to_remove:
    best_cols.remove(x)

Xc_train2 = Xc_train.loc[:, best_cols]
Xc_train_sc2 = Xc_train_sc.loc[:, best_cols]
Xc_test2 = Xc_test.loc[:, best_cols]
Xc_test_sc2 = Xc_test_sc.loc[:, best_cols]

# # # Hyper tuning the models parameters # # #

param_lg = {'penalty': ['l2'], 'C': np.logspace(-4, 4, 9)}
param_rfc = {'n_estimators': range(60, 121, 10), 'max_depth': [5, 6, 7, 8],
             'min_samples_split': [800, 960, 1120, 1280, 1440, 1600], 'min_samples_leaf': range(30, 71, 10)}
param_gbc = {'n_estimators': range(60, 121, 10), 'min_samples_split': [800, 960, 1120, 1280, 1440, 1600],
             'max_depth': [5, 6, 7, 8], 'subsample': [0.7, 0.75, 0.8, 0.85, 0.9]}

possible_param_lg = list(ParameterGrid(param_lg))
possible_param_rfc = list(ParameterGrid(param_rfc))
possible_param_gbc = list(ParameterGrid(param_gbc))

def tune_model(params_possible, estimator, X_train, y_train, X_test, y_test, X_train_sc, X_test_sc, yr):
    print(f'{estimator} is starting')
    results = pd.DataFrame(columns=['Model', 'Average Return', 'SD return', 'Total Amount Invested',
                                    'Total Loans Invested', 'Recall', 'AUC'])
    for params in params_possible:
        if estimator == 'LG':
            model = LogisticRegression(**params)
            y_prob = train_clf(X_train_sc, y_train, X_test_sc, model)
        elif estimator == 'RFC':
            model = RandomForestClassifier(**params)
            y_prob = train_clf(X_train, y_train, X_test, model)
        else:  # estimator == 'GBC'
            model = GradientBoostingClassifier(**params)
            y_prob = train_clf(X_train, y_train, X_test, model)
        temp = results_clf(model, y_prob, X_test, y_test, yr, threshold=0.2)
        results = results.append(temp, ignore_index=True)
        print('Iteration %d done out of %d' % (params_possible.index(params) + 1, len(params_possible)))
    print(f'{estimator} is done')
    return results

tune_results_lg = tune_model(possible_param_lg, 'LG', Xc_train, yc_train, Xc_test, yc_test, Xc_train_sc, Xc_test_sc, y_return_test)
tune_results_rfc = tune_model(possible_param_rfc, 'RFC', Xc_train, yc_train, Xc_test, yc_test, Xc_train_sc, Xc_test_sc, y_return_test)
tune_results_gbc = tune_model(possible_param_gbc, 'GBC', Xc_train, yc_train, Xc_test, yc_test, Xc_train_sc, Xc_test_sc, y_return_test)
tuning_results = pd.concat([tune_results_lg, tune_results_rfc, tune_results_gbc])

# # # Models # # #

LG = LogisticRegression()
RFC = RandomForestClassifier()
GBC = GradientBoostingClassifier()

# # # Cross Validation # # #

def cross_validate(model, df, random_states, scaled=False):
    print(f'{model} is starting')
    result = pd.DataFrame(columns=['Model', 'Average Return', 'SD return', 'Total Amount Invested',
                                   'Total Loans Invested', 'Recall', 'AUC'])
    X = df.drop(['ann_exp_return', 'loan_status'], axis=1)
    y, yr = df['loan_status'], df['ann_exp_return']
    for rand in random_states:
        X_train, X_test, y_train, y_test, yr_train, yr_test = train_test_split(X, y, yr, stratify=y,
                                                                               test_size=0.4, random_state=rand)
        if scaled:
            scaler = StandardScaler()
            X_train_sc = pd.DataFrame(scaler.fit_transform(X_train))
            X_train_sc.columns = Xc_train.columns
            X_test_sc = pd.DataFrame(scaler.transform(X_test))
            X_test_sc.columns, X_test_sc.index = X_test.columns, X_test.index
            y_prob = train_clf(X_train_sc, y_train, X_test_sc, model)
        else:
            y_prob = train_clf(X_train, y_train, X_test, model)
        res = results_clf(model, y_prob, X_test, y_test, yr_test, 0.2, print_results=False)
        res['random_state'] = rand
        result = result.append(res, ignore_index=True)
        print('Iteration %d done out of %d' % (random_states.index(rand) + 1, len(random_states)))
    print(f'{model} is done')
    return result

random_states = [0, 7, 4, 14, 2, 27, 13, 1996, 1995, 1997, 18, 1, 5, 11, 133]
cross_results_lg = cross_validate(LG, df, random_states, scaled=True)
cross_results_rfc = cross_validate(RFC, df, random_states, scaled=False)
cross_results_gbc = cross_validate(GBC, df, random_states, scaled=False)
cross_results = pd.concat([cross_results_lg, cross_results_rfc, cross_results_gbc])
cross_results.to_csv(f'{results_directory}/cross_results.csv')

# # # Investing system and results # # #

def investment(y_prob, yr, amnt_of_loans, budget):
    y_prob_series = pd.Series(y_prob)
    y_prob_series.index = yr.index
    full_pred_df = pd.concat([y_prob_series, yr, amnt_of_loans], axis=1)
    full_pred_df.columns = ['probs', 'ann_exp_return', 'loan_amount']
    full_pred_df.sort_values(by='probs', inplace=True)
    amount, tot_loans = 0, 0
    for row in range(len(full_pred_df.index)):
        new_loan = int(full_pred_df.iloc[row, 2])
        if (amount + new_loan) <= budget:
            amount += new_loan
            tot_loans += 1
        else:
            break

    df_budget = full_pred_df.iloc[list(range(tot_loans)), :]
    tot_inv = np.sum(df_budget['loan_amount'])
    tot_loans_inv = len(df_budget['loan_amount'])
    avg_wght_return = np.sum((df_budget['loan_amount'] / tot_inv) * df_budget['ann_exp_return'])
    sd_wght_return = np.sqrt(np.sum(pow((df_budget['ann_exp_return'] - avg_wght_return), 2)) / len(df_budget['ann_exp_return']))
    max_return = df_budget['ann_exp_return'].max()
    return avg_wght_return, sd_wght_return, max_return, tot_loans_inv, tot_inv

# # # Choosing best model # # #

LG = LogisticRegression()
RFC = RandomForestClassifier()
GBC = GradientBoostingClassifier()
loan_amounts = Xc_test.loc[:, 'loan_amnt']
max_investment = np.sum(loan_amounts)
y_prob_lg = train_clf(Xc_train_sc2, yc_train, Xc_test_sc2, LG)
y_prob_rfc = train_clf(Xc_train2, yc_train, Xc_test2, RFC)
y_prob_gbc = train_clf(Xc_train2, yc_train, Xc_test2, GBC)

budgets = list(np.arange(100000000, max_investment, 100000000))
avg_wghtd_rs_lg, sd_wghtd_rs_lg, max_rs_lg, tot_loans_invs_lg, tot_invs_lg = [], [], [], [], []
avg_wghtd_rs_rfc, sd_wghtd_rs_rfc, max_rs_rfc, tot_loans_invs_rfc, tot_invs_rfc = [], [], [], [], []
avg_wghtd_rs_gbc, sd_wghtd_rs_gbc, max_rs_gbc, tot_loans_invs_gbc, tot_invs_gbc = [], [], [], [], []

for bud in budgets:
    avg_wghtd_r_lg, sd_wghtd_r_lg, max_r_lg, tot_loans_inv_lg, tot_inv_lg = investment(y_prob_lg, y_return_test, loan_amounts, bud)
    avg_wghtd_r_rfc, sd_wghtd_r_rfc, max_r_rfc, tot_loans_inv_rfc, tot_inv_rfc = investment(y_prob_rfc, y_return_test, loan_amounts, bud)
    avg_wghtd_r_gbc, sd_wghtd_r_gbc, max_r_gbc, tot_loans_inv_gbc, tot_inv_gbc = investment(y_prob_gbc, y_return_test, loan_amounts, bud)
    avg_wghtd_rs_lg.append(avg_wghtd_r_lg), avg_wghtd_rs_rfc.append(avg_wghtd_r_rfc), avg_wghtd_rs_gbc.append(avg_wghtd_r_gbc)
    sd_wghtd_rs_lg.append(sd_wghtd_r_lg), sd_wghtd_rs_rfc.append(sd_wghtd_r_rfc), sd_wghtd_rs_gbc.append(sd_wghtd_r_gbc)
    max_rs_lg.append(max_r_lg), max_rs_rfc.append(max_r_rfc), max_rs_gbc.append(max_r_gbc)
    tot_invs_lg.append(tot_inv_lg), tot_invs_rfc.append(tot_inv_rfc), tot_invs_gbc.append(tot_inv_gbc)
    tot_loans_invs_lg.append(tot_loans_inv_lg), tot_loans_invs_rfc.append(tot_loans_inv_rfc), tot_loans_invs_gbc.append(tot_loans_inv_gbc)

models = [LG]*len(budgets) + [RFC]*len(budgets) + [GBC]*len(budgets)
clf_results_df = pd.DataFrame({
    'Portfolios': tot_invs_lg + tot_invs_rfc + tot_invs_gbc,
    'Model': models,
    'Average Weighted Return': avg_wghtd_rs_lg + avg_wghtd_rs_rfc + avg_wghtd_rs_gbc,
    'SD Return': sd_wghtd_rs_lg + sd_wghtd_rs_rfc + sd_wghtd_rs_gbc,
    'Total loans invested': tot_loans_invs_lg + tot_loans_invs_rfc + tot_loans_invs_gbc
})
clf_results_df.to_csv(f'{results_directory}/stage5_clf_results.csv')

ind_max_lg = avg_wghtd_rs_lg.index(max(avg_wghtd_rs_lg))
ind_max_rfc = avg_wghtd_rs_rfc.index(max(avg_wghtd_rs_rfc))
ind_max_gbc = avg_wghtd_rs_gbc.index(max(avg_wghtd_rs_gbc))

plt.plot(budgets, avg_wghtd_rs_lg, color='c', label='Logistic Regression')
plt.plot(budgets[ind_max_lg], avg_wghtd_rs_lg[ind_max_lg], color='c', marker=".", markersize=14)
plt.plot(budgets, avg_wghtd_rs_rfc, color='b', label='Random Forest')
plt.plot(budgets[ind_max_rfc], avg_wghtd_rs_rfc[ind_max_rfc], color='b', marker=".", markersize=14)
plt.plot(budgets, avg_wghtd_rs_gbc, color='g', label='Gradient Boosting')
plt.plot(budgets[ind_max_gbc], avg_wghtd_rs_gbc[ind_max_gbc], color='g', marker=".", markersize=14)
plt.title('Average weighted return per portfolio')
plt.xlabel('Portfolios')
plt.legend(loc="lower left")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage5_avg_returns_weight.png')

print()
plt.plot(budgets, sd_wghtd_rs_lg, color='c', label='Logistic Regression')
plt.plot(budgets, sd_wghtd_rs_rfc, color='b', label='Random Forest')
plt.plot(budgets, sd_wghtd_rs_gbc, color='g', label='Gradient Boosting')
plt.title('Standard Deviation of the returns per portfolio')
plt.xlabel('Portfolios')
plt.legend(loc="lower right")
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage5_sd_returns.png')

print()
def plot_roc_curve(y_test, y_probs, colors, labels):
    plt.figure(figsize=(10, 6))
    plt.plot([0, 1], [0, 1], 'r--')
    for i in range(len(y_probs)):
        fpr, tpr, threshs = roc_curve(y_test, y_probs[i])
        plt.plot(fpr, tpr, colors[i], label=labels[i])
        plt.text(0.6, 0.2-(i*4/100), f'AUC {labels[i]}: {round(roc_auc_score(y_test, y_probs[i]), 2)}')
    plt.legend()
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

plot_roc_curve(yc_test, [y_prob_lg, y_prob_rfc, y_prob_gbc], ['b', 'c', 'g'],
               ['Logistic Regression', 'Random Forest', 'Gradient Boosting Classifer'])
# plt.show()
plt.savefig(get_project_folder() / 'saved_images' / 'plots' / 'stage5_roc_curve.png')

# # # Comparison with grades # # #

df_grades = pd.read_csv(f'{files_directory}/df_grades.csv', index_col=0)

grades_returns = df_grades.groupby('grade').agg(['mean', 'std', 'max'])['ann_exp_return']
grades_status_loan = (df_grades.groupby('grade').agg('sum')['loan_status'])/\
                     (df_grades.groupby('grade').agg('count')['loan_status'])
grades_investment_prop = (df_grades.groupby('grade').agg('sum')['loan_amnt'])/(np.sum(df_grades['loan_amnt']))
summary_grades = pd.concat([grades_returns, grades_status_loan, grades_investment_prop], axis=1)
summary_grades.columns = ['avg_exp_return', 'sd_exp_return', 'max_exp_return', 'prob_default', 'prop_invested']

amnt_loans = Xc_test['loan_amnt']

GBC = GradientBoostingClassifier()
y_prob_gbc = train_clf(Xc_train_sc, yc_train, Xc_test_sc, GBC)

GRADE_A = summary_grades.loc['A', 'prop_invested']
GRADE_B = summary_grades.loc['B', 'prop_invested']
GRADE_C = summary_grades.loc['C', 'prop_invested']
GRADE_D = summary_grades.loc['D', 'prop_invested']
GRADE_E = summary_grades.loc['E', 'prop_invested']
GRADE_F = summary_grades.loc['F', 'prop_invested']
GRADE_G = summary_grades.loc['G', 'prop_invested']

total_amount = sum(amnt_loans)
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_A*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_B*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_C*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_D*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_E*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_F*total_amount))
print(investment(y_prob_gbc, y_return_test, amnt_loans, GRADE_G*total_amount))

# # # save final models # # #
script_directory = get_project_folder() / 'project_scripts'

relevant_cols = ['loan_amnt', 'int_rate', 'annual_inc', 'dti', 'fico_range_low', 'open_acc', 'max_bal_bc',
                    'all_util', 'total_rev_hi_lim', 'inq_fi', 'total_cu_tl', 'avg_cur_bal', 'bc_open_to_buy',
                    'mo_sin_old_rev_tl_op', 'mo_sin_rcnt_rev_tl_op', 'mo_sin_rcnt_tl', 'mort_acc',
                    'mths_since_recent_bc', 'pct_tl_nvr_dlq', 'percent_bc_gt_75', 'total_bal_ex_mort']

X = df.loc[:, relevant_cols]
y = df['loan_status']

filename_gbc = f'{script_directory}/finalized_gbc.sav'
GBC = GradientBoostingClassifier().fit(X, y)
pickle.dump(GBC, open(filename_gbc, 'wb'))
