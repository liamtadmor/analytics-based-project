import pandas as pd
import numpy as np
from pathlib import Path
import re
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.impute import KNNImputer
from scipy.stats import zscore
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.ensemble import GradientBoostingRegressor, RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'
files_directory = get_project_folder() / 'raw_files'  # directory of data file

# # # Read relevant files # # #

stage2_final = pd.read_csv(f'{files_directory}/stage2_final.csv', index_col=1)  # setting loan_id as index
returns = pd.read_csv(f'{files_directory}/returns_2019.csv', index_col=1)
full_2019 = pd.read_csv(f'{files_directory}/loan_status_2019_full.csv', index_col=1)

data_merged = stage2_final.merge(returns.loc[:, 'loan_status'], how='left', left_index=True, right_index=True)
data = data_merged.merge(full_2019.loc[:, 'earliest_cr_line'], how='left', left_index=True, right_index=True)
df = data.drop(['Unnamed: 0', 'term'], axis=1)  # dropping irrelevant for modeling

status_encoding = {'Fully Paid': 0, 'Charged Off': 1}
df = df.replace({'loan_status': status_encoding})  # encoding loan_status to 0/1

# # # Calculating mean, max, min, sd and prob to default of returns per grade # # #

grades_returns = df.groupby('grade').agg(['mean', 'std', 'max', 'min'])['ann_exp_return']
grades_status_loan = (df.groupby('grade').agg('sum')['loan_status'])/(df.groupby('grade').agg('count')['loan_status'])
df_grades = grades_returns.merge(grades_status_loan, how='inner', left_index=True, right_index=True)
df_grades.columns = ['avg_exp_return', 'sd_exp_return', 'max_exp_return', 'min_exp_return', 'prob_default']

# # # Expected returns by status # # #

status_returns = df.groupby('loan_status').agg(['mean', 'std', 'max', 'min'])['ann_exp_return']
returns_above_3perc = df[df['ann_exp_return'] > 0.032]
prop_above_3perc = (returns_above_3perc['loan_status'].value_counts())/(df['loan_status'].value_counts())
print(prop_above_3perc)
returns_above_2perc = df[df['ann_exp_return'] > 0.02]
prop_above_2perc = (returns_above_2perc['loan_status'].value_counts())/(df['loan_status'].value_counts())
print(prop_above_2perc)

# # # Data Preprocessing # # #

# df['int_rate'] = df.apply(lambda row: float(row.int_rate[:-1])/100, axis=1)  # already done in previous stage
df['emp_length'] = df.apply(lambda row: ''.join(re.findall(r'\d+', str(row.emp_length)))
                            if row.emp_length == row.emp_length else row.emp_length, axis=1)  # keeping only the years

df['issue_d'] = pd.to_datetime(df['issue_d'])  # string to datetime
df['earliest_cr_line'] = pd.to_datetime(df['earliest_cr_line'])  # string to datetime
df['mths_from_ear_credit'] = np.round(((df['issue_d'] - df['earliest_cr_line']) / np.timedelta64(1, 'M')))
df.drop(['issue_d', 'earliest_cr_line'], axis=1, inplace=True)

dummies_home = pd.get_dummies(df.loc[:, 'home_ownership'])
fig, ax = plt.subplots(figsize=(14, 10))
sns.heatmap(dummies_home.corr(method='pearson'), annot=True)
plt.title("Pearson Correlation", fontsize=20)
plt.savefig(get_project_folder() / 'saved_images' / 'correlations' / 'home_own_corr.png')

is_home_own = {'MORTGAGE': 0, 'RENT': 0, 'ANY': 0, 'OWN': 1}
df = df.replace({'home_ownership': is_home_own})  # encoding home_ownership (1 if it's own, else 0)

grade_encoding = {'G': 0, 'F': 1, 'E': 2, 'D': 3, 'C': 4, 'B': 5, 'A': 6}
df = df.replace({'grade': grade_encoding})  # encoding grades

df.drop(['emp_title', 'purpose', 'addr_state', 'sub_grade'], axis=1, inplace=True)

# # # Filling missing values with KNN imputer # # #

sc_imputer = StandardScaler()
df_sc = sc_imputer.fit_transform(df)  # scaling data
imputer = KNNImputer()  # knn imputation model
imputer.fit(df_sc)
arr_knn_imputed = imputer.transform(df_sc)
df_knn_imputed = pd.DataFrame(sc_imputer.inverse_transform(arr_knn_imputed), columns=df.columns)  # descaling
print('Missing values before knn imputer: %d' % df.isna().sum().sum())  # count na's before imputation
print('Missing values after knn imputer: %d' % df_knn_imputed.isna().sum().sum())  # count na's after imputation

df_knn_imputed.index = df.index
df_knn_imputed.to_csv(f'{files_directory}/stage3_after_nas.csv')

df1 = pd.read_csv(f'{files_directory}/stage3_after_nas.csv', index_col=0)

# df1 = df.fillna(df.median())
# df1 = df_knn_imputed.copy()

# # # Handling outliers # # #
Q1 = df1.quantile(0.25)
Q3 = df1.quantile(0.75)
IQR = Q3 - Q1
df1_outliers_iqr = (df1 < (Q1 - 1.5 * IQR)) | (df1 > (Q3 + 1.5 * IQR))
df1_outliers_gauss = pd.DataFrame(np.abs(zscore(df1)) > 3)
df1_outliers_gauss.columns = df1.columns
columns, iqr_outliers, gauss_outliers = [], [], []
for col in df1_outliers_iqr.columns:
    columns.append(col)
    iqr_outliers.append(df1_outliers_iqr[col].sum())
    gauss_outliers.append(df1_outliers_gauss[col].sum())

prop_outliers_iqr = np.array(iqr_outliers)/len(df1_outliers_iqr.index)
prop_outliers_gauss = np.array(gauss_outliers)/len(df1_outliers_gauss.index)
df_outliers = pd.DataFrame({'Columns': columns, 'IQR Outliers': iqr_outliers, 'Prop IQR Outliers': prop_outliers_iqr,
                            'Gaussian Outliers': gauss_outliers, 'Prop Gauss Outliers': prop_outliers_gauss})

df1_inliers_gauss = pd.DataFrame(np.abs(zscore(df1)) <= 3)
df1_inliers_gauss.columns, df1_inliers_gauss.index = df1.columns, df1.index
df2 = df1.copy()
for col in df2.columns:
    if col == 'ann_exp_return':
        pass
    else:
        df2 = df2[df1_inliers_gauss[col]]

print(df2['loan_status'].value_counts())

# # # Regression models # # #

# train/test split
X_reg = df1.drop(['loan_status', 'ann_exp_return'], axis=1)
y_reg = df1['ann_exp_return']
Xr_train, Xr_test, yr_train, yr_test = train_test_split(X_reg, y_reg, test_size=0.4)

# linear regression
pipe_LR = make_pipeline(StandardScaler(), LinearRegression())
pipe_LR.fit(Xr_train, yr_train)
y_pred_lr = pipe_LR.predict(Xr_test)
print('LR R-squared: %.4f' % r2_score(yr_test, y_pred_lr))
print('LR mse: %.4f' % mean_squared_error(yr_test, y_pred_lr))

exp_return_inv_lr = np.mean(y_pred_lr[y_pred_lr > 0])
print(exp_return_inv_lr)

# gradient boosting regression
GBR = GradientBoostingRegressor()
GBR.fit(Xr_train, yr_train)
y_pred_gbr = GBR.predict(Xr_test)
print('GBR R-squared: %.4f' % r2_score(yr_test, y_pred_gbr))
print('GBR mse: %.4f' % mean_squared_error(yr_test, y_pred_gbr))

exp_return_inv_rfr = np.mean(y_pred_gbr[y_pred_gbr > 0])
print(exp_return_inv_rfr)

# # # Classification models # # #

# average return per class
avg_return_fullypaid = status_returns.loc[0, 'mean']
avg_return_chargedoff = status_returns.loc[1, 'mean']

# train/test split
X_clf = df1.drop(['loan_status', 'ann_exp_return'], axis=1)
y_clf = df1['loan_status'].astype(int)
Xc_train, Xc_test, yc_train, yc_test = train_test_split(X_clf, y_clf, test_size=0.4, stratify=y_clf)

# logistic regression
pipe_LG = make_pipeline(StandardScaler(), LogisticRegression())
pipe_LG.fit(Xc_train, yc_train)
y_pred_lg = pipe_LG.predict(Xc_test)
print('LG recall: %.4f' % recall_score(yc_test, y_pred_lg))
print('LG roc_auc_score: %.4f' % roc_auc_score(yc_test, y_pred_lg))
confusion_matrix_pred_lg = pd.DataFrame(confusion_matrix(yc_test, y_pred_lg),
                                        columns=pd.MultiIndex.from_product([['Prediction'], ['Fully Paid', 'Charged Off']]),
                                        index=pd.MultiIndex.from_product([['Actual'], ['Fully Paid', 'Charged Off']]))
print(confusion_matrix_pred_lg)

inv_actual_0_lg, inv_actual_1_lg = confusion_matrix_pred_lg.iloc[0, 0], confusion_matrix_pred_lg.iloc[1, 0]
total_inv_lg = inv_actual_0_lg + inv_actual_1_lg

prob_pred_fullypaid_actual_chargedoff_lg = inv_actual_1_lg/total_inv_lg
prob_pred_fullypaid_actual_fullypaid_lg = inv_actual_0_lg/total_inv_lg
exp_return_inv_lg = prob_pred_fullypaid_actual_fullypaid_lg * avg_return_fullypaid + \
                    prob_pred_fullypaid_actual_chargedoff_lg * avg_return_chargedoff
print(exp_return_inv_lg)

# random forest classifier
RFC = RandomForestClassifier()
RFC.fit(Xc_train, yc_train)
y_pred_rfc = RFC.predict(Xc_test)
print('RFC recall: %.4f' % recall_score(yc_test, y_pred_rfc))
print('RFC roc_auc_score: %.4f' % roc_auc_score(yc_test, y_pred_rfc))
confusion_matrix_pred_rfc= pd.DataFrame(confusion_matrix(yc_test, y_pred_rfc),
                                        columns=pd.MultiIndex.from_product([['Prediction'], ['Fully Paid', 'Charged Off']]),
                                        index=pd.MultiIndex.from_product([['Actual'], ['Fully Paid', 'Charged Off']]))
print(confusion_matrix_pred_rfc)

inv_actual_0_rfc, inv_actual_1_rfc = confusion_matrix_pred_rfc.iloc[0, 0], confusion_matrix_pred_rfc.iloc[1, 0]
total_inv_rfc = inv_actual_0_rfc + inv_actual_1_rfc

prob_pred_fullypaid_actual_chargedoff_rfc = inv_actual_1_rfc/total_inv_rfc
prob_pred_fullypaid_actual_fullypaid_rfc = inv_actual_0_rfc/total_inv_rfc
exp_return_inv_rfc = prob_pred_fullypaid_actual_fullypaid_rfc * avg_return_fullypaid + \
                     prob_pred_fullypaid_actual_chargedoff_rfc * avg_return_chargedoff
print(exp_return_inv_rfc)

# knn classifier
pipe_KNN = make_pipeline(StandardScaler(), KNeighborsClassifier())
pipe_KNN.fit(Xc_train, yc_train)
y_pred_knn = pipe_KNN.predict(Xc_test)
print('KNN recall: %.4f' % recall_score(yc_test, y_pred_knn))
print('KNN roc_auc_score: %.4f' % roc_auc_score(yc_test, y_pred_knn))
confusion_matrix_pred_knn = pd.DataFrame(confusion_matrix(yc_test, y_pred_knn),
                                         columns=pd.MultiIndex.from_product([['Prediction'], ['Fully Paid', 'Charged Off']]),
                                         index=pd.MultiIndex.from_product([['Actual'], ['Fully Paid', 'Charged Off']]))
print(confusion_matrix_pred_knn)

inv_actual_0_knn, inv_actual_1_knn = confusion_matrix_pred_knn.iloc[0, 0], confusion_matrix_pred_knn.iloc[1, 0]
total_inv_knn = inv_actual_0_knn + inv_actual_1_knn

prob_pred_fullypaid_actual_chargedoff_knn = inv_actual_1_knn/total_inv_knn
prob_pred_fullypaid_actual_fullypaid_knn = inv_actual_0_knn/total_inv_knn
exp_return_inv_knn = prob_pred_fullypaid_actual_fullypaid_knn * avg_return_fullypaid + \
                     prob_pred_fullypaid_actual_chargedoff_knn * avg_return_chargedoff
print(exp_return_inv_knn)
