import pandas as pd
from collections import defaultdict
import itertools as it
from pathlib import Path
import argparse
import os

def get_project_folder() -> Path:
    return Path.home() / 'Desktop' / 'analytics-based-project'


def get_path_of_file(file) -> str:           # if file somewhere unknown in laptop
    for root, dirs, files in os.walk("c:\\"):
        for name in files:
            if name == file_name:
                return os.path.abspath(os.path.join(root, name))


def get_dict_from_excel(filename) -> dict:
    xl_file = pd.ExcelFile(filename)
    total_sheets = len(xl_file.sheet_names)
    dicts = []
    for i in range(total_sheets):
        df = pd.read_excel(filename, sheet_name=i, skipfooter=2)
        df_dict = dict(zip(df.iloc[:, 0], df.iloc[:, 1]))
        dicts.append(df_dict)
    super_dict = defaultdict(set)
    for d in dicts:
        for k, v in d.items():
            super_dict[k].add(v)
    return super_dict


def get_possible_params(param_grid):
    list_of_params = []
    combinations = it.product(*(param_grid[key] for key in param_grid))
    param_keys = list(param_grid.keys())
    for tup in combinations:
        new_dict = {}
        for key in range(len(param_keys)):
            new_dict[param_keys[key]] = tup[key]
        list_of_params.append(new_dict)
    return list_of_params


def run_dict(word: str, filename):
    path_file = get_project_folder() / 'raw_files' / filename
    dict1 = get_dict_from_excel(path_file)
    print(dict1[word])


file_name = 'Data Dictionary.xlsx'

print(run_dict('last_fico_range_low', file_name))
print(run_dict('last_fico_range_high', file_name))

parser = argparse.ArgumentParser(description='temp')
parser.add_argument('--run', type=str, required=True)
parser.add_argument('--word', type=str)

args = parser.parse_args()
if args.run == 'run_dict':
    run_dict(file_name, args.word)
